# Contribuer

Le principal objectif de ce projet est de faciliter la collaboration,
donc toute contribution est bienvenue.
Même les personnes ayant des connaissances limitées
dans tout ce qui est « code » et « informatique »
sont encouragées à contribuer.
Seules de la bonne volonté, et un peu de patience pour les personnes peu expérimentées,
sont nécessaires.

## Sur quoi contribuer

Le projet a besoin de contributions sur
la copie des sujets, la rédaction de corrections,
l'élaboration des conventions et recommendations par rapport à la rédaction,
mais aussi sur des sujets techniques comme le moteur de rendu
et le HTML généré,
et enfin de nouvelles idées, suggestions et commentaires.

- la priorité est l'ajout de contenu, c'est à dire les sujets et les corrections.
Contribuer à la copie des sujets est de loin le plus facile,
il faut juste que le code soit correct et reproduise l'énoncé initial.
Il y a simplement une liste de conventions à suivre
pour assurer une certaine qualité et uniformité dans le code.
Certains éléments des sujets peuvent tout de même parfois être un peu techniques à reproduire
(tableaux et illustrations, typiquement);
les contributeurs débutants peuvent se concentrer sur les exercices
qui ne comportent pas ce genre d'éléments,
ou ne rédiger que la partie "facile" d'un exercice
et confier les éléments plus techniques à un contributeur plus expérimenté;
mais ils peuvent aussi demander de l'aide pour apprendre à réaliser ce genre d'éléments !

- Pour les **corrections** des exercices, en plus de la technique
il est demandé aux contributeurs de porter soin au style d'écriture et à la pédagogie.
Cela ne veut pas dire que les propositions de corrections d'exercices non encore corrigés
seront  difficilement acceptées:
une correction même avec des défauts vaut mieux que pas de correction du tout
(il faut tout de même qu'elle soit valide !).
En revanche il est demandé aux contributeurs d'accepter que leur corrections soient critiquées,
et possiblement modifiées.
Évidemment les critiques doivent être respectueuses, argumentées, etc.

- Les contributions à l'élaboration des conventions et recommandations citées précédemment
sont aussi les bienvenues.

- Pour n'importe quelle idée, remarque, question,
ou si vous remarquez un bug, n'hésitez pas à créer une *“issue”* sur le projet GitLab:
https://gitlab.com/cedricvanrompay/annales-brevet-et-bac/issues/new


## Comment contribuer

Idéalement les contributions devraient être faites
via des *“merge request”* sur la plate-forme GitLab.com sur laquelle le projet est hébergé,
et les contributeurs devraient tester le rendu de leur contribution
sur leur propre machine avant de la soumettre au projet.

Évidemment si ce projet veut pouvoir accueillir des contributeurs
même sans beaucoup d'expérience il faut qu'il y ait des moyens plus simples de contribuer.
Pour cela, un guide “contribuer de façon simple” sera bientôt disponible.

Ce qui suit s'applique à toutes la méthodes de contribution:

- Vous allez avoir besoin d'un compte (gratuit) sur GitLab.com .
Ce serait toujours possible de faire sans en envoyant vos contributions directement à un autre contributeur qui la posterai en votre nom, mais ça n'est pas idéal.

- les règles à suivre pour l'écriture des exercices et des corrections
et la soumission des contributions
se trouvent dans le [wiki du projet][wiki].

[wiki]: https://gitlab.com/cedricvanrompay/annales-brevet-et-bac/wikis/home
