.. |br| raw:: html

   <br />

Question 1
==========================

Corinne choisit le nombre 1.
En soustrayant 3 elle obtient 1-3 = 2,
puis le carré de 2 est 4,
elle obtient donc 4.


Question 2
==========================

Le carré de -5 est 25;
le triple de -5 est égal à -15,
donc en ajoutant l'un à l'autre on obtient 25 - 15 = 10.
Puis on ajoute 7 pour obtenir le résultat final de 17.


Question 3
==========================

Lina a saisie la formule ``B1^2 + B1*3 + 7``


Question 4.a
==========================

Le résultat du programme A en fonction de :math:`x`
peut s'écrire
:math:`(x - 3)^2`

Et on a
:math:`(x - 3)^2`
= :math:`(x - 3) × (x - 3)`
= :math:`x×(x - 3) - 3(x - 3)`
= :math:`x^2 - 3x - 3x + 9`
= :math:`x^2 - 6x + 9`


Question 4.b
==========================

On a
:math:`B(x)`
= :math:`x^2 + 3x + 7`


Question 4.c
==========================

On cherche :math:`x`
tel que :math:`A(x) = B(x)`,
c'est à dire tel que:

:math:`\cancel{x^2} - 6x + 9 = \cancel{x^2} + 3x + 7` |br|
:math:`9 - 7 = 3x + 6x` |br|
:math:`2 = 9x` |br|
:math:`x = \frac{2}{9}`

Le nombre de départ pour lequel les deux programmes donnent le même résultat
est donc :math:`\frac{2}{9}`.
