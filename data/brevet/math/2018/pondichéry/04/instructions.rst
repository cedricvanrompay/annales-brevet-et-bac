**Programme A :**

- Choisir un nombre
- Soustraire 3
- Calculer le carré du résultat obtenu

**Programme B :**

- Choisir un nombre
- Calculer le carré de ce nombre
- Ajouter le triple du nombre de départ
- Ajouter 7

1. Corinne choisit le nombre 1 et applique le programme A.
   Expliquer en détaillant les calculs que
   le résultat du programme de calcul est 4.

2. Tidjane choisit le nombre −5 et applique le programme B.
   Quel résultat obtient-il ?

3. Lina souhaite regrouper le résultat de chaque programme à l’aide d’un tableur.
   Elle crée la feuille de calcul ci-dessous.
   Quelle formule, copiée ensuite à droite dans les cellules C3 à H3,
   a-t-elle saisie dans la cellule B3 ?

   .. image:: ../figures/exo-04-fig-01.svg

4. Zoé cherche à trouver un nombre de départ
   pour lequel les deux programmes de calcul donnent le même résultat.
   Pour cela, elle appelle :math:`x` le nombre choisi au départ
   et exprime le résultat de chaque programme de calcul en fonction de :math:`x`.

   a. Montrer que le résultat du programme A en fonction de :math:`x`
      peut s’écrire sous forme développée et réduite :
      :math:`x^2 − 6x + 9`.

   b. Écrire le résultat du programme B en fonction de :math:`x`.

   c. Existe-t-il un nombre de départ
      pour lequel les deux programmes donnent le même résultat ?
      Si oui, lequel ?
