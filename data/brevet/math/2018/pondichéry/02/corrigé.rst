Question 1
======================

La transformation géométrique qui permet d'obtenir le motif 2
à partir du motif 1 dans la figure 1
s'appelle une translation.


Question 2
======================

Dans la figure 2 on voit qu'un motif pied-de-coq
est composé de 4 carreaux entiers
et 8 demi-carreaux.
Chaque carreau a une aire de 1 cm²,
donc l'aire d'un motif pied-de-coq
est de 4×1 + 8×½ = 8 cm².


Question 3
======================

Quand on divise par 2 les longueurs d’un carreau,
son aire n'est pas divisé par 2 mais par 2×2 = 4.
Donc l'aire du motif sera divisé par 4 aussi.
Marie a tort.
