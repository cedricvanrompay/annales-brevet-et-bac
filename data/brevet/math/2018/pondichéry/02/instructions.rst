Le pavage représenté sur la figure 1
est réalisé à partir d’un motif appelé pied-de-coq
qui est présent sur de nombreux tissus
utilisés pour la fabrication de vêtements.

.. image:: ../figures/exo-02-fig-01.svg
    :class: max-width-30-em

Le motif pied-de-coq est représenté par le polygone ci-dessous (figure 2)
qui peut être réalisé à l’aide d’un quadrillage régulier.

.. image:: ../figures/exo-02-fig-02.svg
    :class: max-width-30-em

1. Sur la figure 1,
   quel type de transformation géométrique
   permet d’obtenir le motif 2 à partir du motif 1 ?

2. Dans cette question, on considère que : AB = 1 cm (figure 2).
   Déterminer l’aire d’un motif pied-de-coq.

3. Marie affirme
   « si je divise par 2 les longueurs d’un motif,
   son aire sera aussi divisée par 2 ».
   A-t-elle raison ? Expliquer pourquoi.
