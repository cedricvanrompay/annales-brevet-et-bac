Question 1
======================

Il y a 13 cases
et la boule a la même probabilité de s'arrêter sur chacune des cases.
La probabilité que la boule s’arrête sur la case numérotée 8
est donc de :math:`\frac{1}{13}`.


Question 2
======================

Il y a 6 cases ayant un numéro impair (1, 3, 5, 7, 9 et 11),
la probabilité que la boule s’arrête sur une case ayant un numéro impair
est donc de :math:`\frac{6}{13}`.


Question 3
======================

Il y a 5 cases dont le numéro dest un nombre premier (1, 3, 5, 7, 11),
la probabilité que la boule s’arrête sur une case dont le numéro est un nombre premier
est donc de :math:`\frac{5}{13}`.


Question 4
======================

Chaque lancer est indépendant des autres lancers,
donc la boule a la même probabilité de s'arrêter sur la case numérotée 9
que sur la case numérotée 7,
peu importe le résultat des lancers précédents.
