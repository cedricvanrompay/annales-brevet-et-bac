On considère un jeu composé d’un plateau tournant et d’une boule.
Représenté ci-dessous, ce plateau comporte 13 cases numérotées de 0 à 12.

.. image:: ../figures/exo-01-fig-01.svg
   :class: max-width-30-em

On lance la boule sur le plateau.
La boule finit par s’arrêter au hasard sur une case numérotée.

La boule a la même probabilité de s’arrêter sur chaque case.

1. Quelle est la probabilité que
   la boule s’arrête sur la case numérotée 8 ?

2. Quelle est la probabilité que
   le numéro de la case sur lequel la boule s’arrête
   soit un nombre impair ?

3. Quelle est la probabilité que
   le numéro de la case sur laquelle la boule s’arrête
   soit un nombre premier ?

4. Lors des deux derniers lancers,
   la boule s’est arrêtée à chaque fois sur la case numérotée 9.
   A-t-on maintenant plus de chances que la boule s’arrête sur la case numérotée 9
   plutôt que sur la case numérotée 7 ?
   Argumenter à l’aide d’un calcul de probabilités.
