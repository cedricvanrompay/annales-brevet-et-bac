Chris fait une course à vélo tout terrain (VTT).
Le graphique ci-dessous représente sa fréquence cardiaque
(en battements par minute)
en fonction du temps lors de la course.

.. image:: ../figures/exo-06-fig-01.png

1. Quelle est la fréquence cardiaque de Chris au départ de sa course ?

2. Quel est le maximum de la fréquence cardiaque atteinte par Chris au cours de sa course ?

3. Chris est parti à 9 h 33 de chez lui et termine sa course à 10 h 26.
   Quelle a été la durée, en minutes, de sa course ?

4. Chris a parcouru 11 km lors de cette course.
   Montrer que sa vitesse moyenne est d’environ 12,5 km/h.

5. On appelle FCM (Fréquence Cardiaque Maximale)
   la fréquence maximale que peut supporter l’organisme.
   Celle de Chris est FCM = 190 battements par minute.
   En effectuant des recherches sur des sites internet spécialisés,
   il a trouvé le tableau suivant.

   +---------------------+---------------+-----------+-----------+-----------------+
   | Effort              | léger         | soutenu   | tempo     | seuil anaérobie |
   +---------------------+---------------+-----------+-----------+-----------------+
   | Fréquence cardiaque | Inférieure à  | 70 à 85%  | 85 à 92%  | 92 à 97%        |
   | mesurée             | 70% de la FCM | de la FCM | de la FCM | de la FCM       |
   +---------------------+---------------+-----------+-----------+-----------------+

    Estimer la durée de la période pendant laquelle
    Chris a fourni un effort soutenu au cours de sa course.
