Question 1
===========================

La fréquence cardiaque de Chris au départ de sa course
est d'environ 52 battements par minute.


Question 2
===========================

Le maximum de la fréquence cardiaque atteinte par Chris au cours de sa course
est d'environ 160 battements par minute.


Question 3
===========================

La durée de la course Chris est de
(60 - 33) + 26 = 53 minutes.


Question 4
===========================

53 minutes correpondent à :math:`\frac{53}{60}` heures,
donc la vitesse moyenne de Chris est de
:math:`\frac{11}{\frac{53}{60}}`
= :math:`\frac{11 × 60}{53}`
:math:`\approx 12,45` km/h

Ce qui arrondi au dixième près donne 12,5 km/h.


Question 5
===========================

Un effort soutenu correspond à une fréquence cardiaque entre 70% et 85% de la FCM,
soit pour Chris entre
:math:`\frac{70}{100} × 190 = 133`
et :math:`\frac{85}{100} × 190 = 161.5` battements par minute.

Il a été dans cette intervalle depuis la 8ème minute de course jusqu'à la 42ème environ,
soit une durée de 34 minutes.
