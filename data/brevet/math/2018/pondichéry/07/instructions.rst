.. image:: ../figures/exo-07-fig-01.svg
   :class: max-width-30-em

On considère ci-dessus un triangle ABC rectangle en A
tel que :math:`\widehat{ABC}` = 30° et AB =  7 cm.
H est le pied de la hauteur issue de A.

1. Tracer la figure en vraie grandeur sur la copie.
   Laisser les traits de construction apparents sur la copie.

2. Démontrer que AH = 3,5 cm.

3. Démontrer que les triangles ABC et HAC sont semblables.

4. Déterminer le coefficient de réduction
   permettant de passer du triangle ABC au triangle HAC.
