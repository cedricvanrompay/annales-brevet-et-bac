Question 1
===========================

*étapes pour dessiner la figure:*

- tracer un trait horizontal de 7 cm pour faire le segment [AB]
- tracer en trait fin (sans appuyer fort) une demi-droite verticale partant de A et allant vers le haut,
  en s'aidant de l'équerre pour faire un angle droit avec AB
- avec le raporteur centré sur B, marquer un point à 30° depuis (AB) 
- Tracer un trait partant B, passant par le point fait avec le rapporteur,
  et s'arrêtant au trait fin de la droite verticale partant de A
  pour faire le segment [BC]
- tracer le segment [AC] en repassant par dessus la droite verticale
- avec l'équerre, trouver la droite perpendiculaire à (BC) qui passe par A,
  et s'en servir pour tracer [AH]


Question 2
===========================

Le triangle AHB est rectangle en H,
on a donc :math:`\sin(\widehat{ABH}) = \frac{AH}{AB}`
et donc :math:`AH = \sin(\widehat{ABH}) × AB`
= :math:`\sin(30) × 7`
= :math:`\frac{1}{2} × 7`
= 3,5 cm


Question 3
===========================

Les angles :math:`\widehat{ACH}` et :math:`\widehat{ACB}` sont égaux par construction,
et les angles :math:`\widehat{CAB}` et :math:`\widehat{CHA}` sont égaux
parce qu'ils sont tous les deux droits.

Puis, comme la somme des angles d'un triangle fait toujours 180°,
on a :math:`\widehat{CAH}` = 180 - :math:`\widehat{CHA}` - :math:`\widehat{ACH}`
et :math:`\widehat{CBA}` = 180 - :math:`\widehat{CAB}` - :math:`\widehat{ACB}`

Avec les éalités précédentes on obtient :math:`\widehat{CAH}` = :math:`\widehat{CBA}`.
Les triangles ABC et AHC ont donc les mêmes angles,
ils sont donc semblables.


Question 4
===========================

Le coefficient de réduction permettant de passer du triangle ABC au triangle HAC
est égal au rapport entre la longueur de AB et la longueur de AH
(qui sont le deuxième plus grand côté de chaque triangle),
soit un rapport de :math:`\frac{AH}{AB}` = :math:`\frac{3,5}{7}` = :math:`\frac{1}{2}`.
