Dans tout l’exercice, l’unité de longueur est le mm.

On lance une fléchette sur une plaque carrée
sur laquelle figure une cible circulaire (en gris sur la figure).
Si la pointe de la fléchette est sur le bord de la cible,
on considère que la cible n’est pas atteinte.

.. image:: ../figures/exo-05-fig-01.svg
   :class: max-width-30-em

On considère que cette expérience est aléatoire
et l’on s’intéresse à la probabilité que la fléchette atteigne la cible.

- La longueur du côté de la plaque carrée est 200.
- Le rayon de la cible est 100.
- La fléchette est représentée par le point F
  de coordonnées :math:`(x ; y)`
  où :math:`x` et :math:`y` sont des nombres aléatoires compris entre −100 et 100.

1. Dans l’exemple ci-dessus,
   la fléchette F est située au point de coordonnées :math:`(72 ; 54)`.
   Montrer que la distance OF, entre la fléchette et l’origine du repère, est 90.

2. D’une façon générale,
   quel nombre ne doit pas dépasser la distance OF
   pour que la fléchette atteigne la cible ?

3. On réalise un programme
   qui simule plusieurs fois le lancer de cette fléchette sur la plaque carrée
   et qui compte le nombre de lancers atteignant la cible.
   Le programmeur a créé trois variables nommées :
   ``carré de OF``, ``distance`` et ``score``.

   .. image:: ../figures/exo-05-fig-02.png

   a. Lorsqu’on exécute ce programme, combien de lancers sont simulés ?

   b. Quel est le rôle de la variable `score` ?

   c. Compléter et recopier sur la copie uniquement
      les lignes 5, 6 et 7 du programme
      afin qu’il fonctionne correctement.

   d. Après une exécution du programme, la variable score est égale à 102.
      A quelle fréquence la cible a-t-elle été atteinte dans cette simulation ?
      Exprimer le résultat sous la forme d’une fraction irréductible.

4. On admet que la probabilité d’atteindre la cible est égale au quotient :
   aire de la cible divisée par aire de la plaque carrée.
   Donner une valeur approchée de cette probabilité au centième près.
