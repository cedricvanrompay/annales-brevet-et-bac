Question 1
==========================

Le triangle OFH est rectangle en H
donc, d'après le théorème de Pythagore on a :

:math:`OF^2`
= :math:`OH^2 + FH^2`
= :math:`72^2 + 54^2`
= :math:`5184 + 2916`
= :math:`8100`

Or on a bien :math:`90^2 = 8100`
donc :math:`OF = 90`.


Question 2
==========================

Pour que la fléchette atteigne la cible
la distance OF ne doit pas dépasser le rayon du disque,
c'est à dire 100.


Question 3.a
==========================

Lorsqu’on exécute ce programme, 120 lancers sont simulés
(grâce à la boucle « répéter 120 fois »).


Question 3.b
==========================

La variable « score » compte le nombre de fois où la fléchette a atteint la cible.


Question 3.c
==========================

- ligne 5: mettre « carré de OF » à ( « abscisse x » * « abscisse x » + « abscisse y » * « abscisse y »)
- ligne 6: mettre « distance » à racine de « carré de OF »
- ligne 7: si distance < 100 alors


Question 4
==========================

L'aire de la cible est de
:math:`π×R^2`
et celui de la plaque carrée est de :math:`(2R)^2`,
où :math:`R` est le rayon de la cible.

La probabilité d'atteindre la cible est donc de
:math:`\frac{π×R^2}{(2R)^2}`
= :math:`\frac{π×\cancel{R}^2}{4×\cancel{R}^2}`
= :math:`\frac{π}{4}`
:math:`\approx 0,785`,
soit 0,79 arrondi au centième près.
