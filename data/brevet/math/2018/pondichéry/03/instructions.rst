Cet exercice est un QCM (Questionnaire à choix multiples).
Pour chacune des questions,
quatre réponses sont proposées et une seule est exacte.
Une réponse fausse ou absente n’enlève pas de point.

Pour chacune des trois questions,
écrire sur votre copie le numéro de la question
et la lettre correspondant à la bonne réponse.

1. :math:`2,53 × 10^{15} =`

   a. 2,530 000 000 000 000 00
   b. 2 530 000 000 000 000
   c. 253 000 000 000 000 000
   d. 37,95

2. La latitude de l'équateur est :

   a. 0°
   b. 90° Est
   c. 90° Nord
   d. 90° Sud

3. :math:`\frac{ \frac{2}{3} + \frac{5}{6}}{7} =`

   a. :math:`\frac{3}{14}`
   b. :math:`\frac{1}{9}`
   c. 0,214 285 714
   d. 0,111 111 111
