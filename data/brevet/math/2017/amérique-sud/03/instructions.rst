Léo choisit un nombre, le multiplie par 6 puis ajoute 5.
Julie choisit le même nombre, lui ajoute 8,
multiplie le résultat par le nombre de départ,
puis soustrait le carré du nombre de départ.

1. Léo et Julie choisissent au départ le nombre −3.

   a. Quel résultat obtient Léo ?
   b. Quel résultat obtient Julie ?

2. Quel nombre positif doivent-ils choisir au départ pour obtenir le même résultat ?
