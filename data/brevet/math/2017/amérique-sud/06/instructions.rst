Le bloc d’instruction « carré » ci-dessous
a été programmé puis utilisé dans les deux programmes ci-dessous :

.. image:: ../figures/exo-06-figure-01.png
   :class: max-width-20-em

**Rappel :** L’instruction « avancer de 10 »
fait avancer le lutin de 10 pixels.

**Programme n°1 :**

.. image:: ../figures/exo-06-figure-02.png
   :class: max-width-30-em

**Programme n°2 :**

.. image:: ../figures/exo-06-figure-03.png
   :class: max-width-30-em

1. Voici trois dessins :

   .. image:: ../figures/exo-06-figure-04.svg

   a. Lequel de ces trois dessins obtient-on avec le programme n°1 ?
   b. Lequel de ces trois dessins obtient-on avec le programme n°2 ?
   c. Pour chacun des deux programmes,
      déterminer la longueur, en pixel, du côté du plus grand carré dessiné.

2. On souhaite modifier le programme n°2
   pour obtenir le dessin ci-dessous.

   .. image:: ../figures/exo-06-figure-05.svg

  Parmi les trois modifications suivantes,
  laquelle permet d’obtenir le dessin souhaité ?
  Aucune justification n’est attendue pour cette question.

  **Modification 1**

  .. image:: ../figures/exo-06-figure-06.png
   :class: max-width-30-em

  **Modification 2**

  .. image:: ../figures/exo-06-figure-07.png
   :class: max-width-30-em

  **Modification 3**

  .. image:: ../figures/exo-06-figure-08.png
   :class: max-width-30-em

