Indiquer en justifiant si chacune des affirmations suivantes est vraie ou fausse.

**Affirmation 1 :** « Les nombres 11 et 13 n’ont aucun multiple commun. »

**Affirmation 2 :** « Le nombre 231 est un nombre premier. »

**Affirmation 3 :** « :math:`\frac{2}{15}` est le tiers de :math:`\frac{6}{15}`. »

**Affirmation 4 :** « :math:`15 − 5 × 7 + 3 = 73` .»

**Affirmation 5 :** « Le triangle ABC avec AB = 4,5 cm, BC = 6 cm et AC = 7,5 cm est rectangle en B. »
