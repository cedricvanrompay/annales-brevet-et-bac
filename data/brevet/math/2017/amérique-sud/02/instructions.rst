Madame Duchemin a aménagé un studio dans les combles de sa maison,
ces combles ayant la forme d’un prisme droit
avec comme base le triangle ABC isocèle en C.

Elle a pris quelques mesures,
au cm près pour les longueurs et au degré près pour les angles.
Elle les a reportées sur le dessin ci-dessous représentant les combles,
ce dessin n’est pas à l’échelle.

.. image:: ../figures/exo-02-figure-01.svg

Madame Duchemin souhaite louer son studio.

Les prix de loyer autorisés dans son quartier sont au maximum de
20 € par m² de surface habitable.
Une surface est dite habitable si
la hauteur sous plafond est de plus de 1,80 m
(article R111 − 2 du code de construction) :
cela correspond à la partie grisée sur la figure.

Madame Duchemin souhaite fixer le prix du loyer à 700 €.
Peut-elle louer son studio à ce prix ?
