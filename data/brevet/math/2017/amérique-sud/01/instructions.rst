Dans une urne, il y a huit boules indiscernables au toucher, qui portent chacune un numéro :

.. image:: ../figures/exo-01-figure-01.svg

1. Si on tire au hasard une boule dans cette urne,
   quelle est la probabilité qu’elle porte le numéro 7 ?

2. Wacim s’apprête à tirer une boule.
   Il affirme qu’il a plus de chance de tirer un numéro pair
   qu’un numéro impair.

   A-t-il raison ?

3. Finalement, Wacim a tiré la boule portant le numéro 5 et la garde :
   il ne la remet pas dans l’urne.
   Baptiste s’apprête à tirer une boule dans l’urne.

   Quelle est la probabilité que cette boule porte le numéro 7 ?
