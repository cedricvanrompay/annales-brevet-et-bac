Le tableau ci-dessous
indique l’apport énergétique en kilocalories par gramme (kcal/g)
de quelques nutriments.

+---------------------------------------------+
| Apport énergétique pour quelques nutriments |
+---------------------------------------------+
| Lipides               |  9 kcal/g           |
+-----------------------+---------------------+
| Protéines             | 4 kcal/g            |
+-----------------------+---------------------+
| Glucides              | 4 kcal/g            |
+-----------------------+---------------------+

1. Un œuf de 50 g est composé de :

   - 5,3 g de lipides ;
   - 6,4 g de protéines ;
   - 0,6 g de glucides ;
   - 37,7 g d’autres éléments non énergétiques.

   Calculer la valeur énergétique totale de cet œuf en kcal.

2. On a retrouvé une partie de l’étiquette d’une tablette de chocolat.
   Dans cette tablette de 200 g de chocolat, quelle est la masse de glucides ?

   .. image:: ../figures/exo-07-figure-01.svg
