Cet exercice porte sur la consommation d’énergie en France.

Le tableau ci-dessous donne la répartition (exprimée en pourcentages)
de la consommation des différents types d’énergie entre 1973 et 2014.

+---------------+------+------+------+------+------+
|               | 1973 | 1980 | 1990 | 2002 | 2014 |
+---------------+------+------+------+------+------+
| Électricité   | 4,3  | 11,7 | 36,4 | 41,7 | 45,4 |
+---------------+------+------+------+------+------+
| Pétrole       | 67,6 | 56,4 | 38,7 | 34,6 | 30,2 |
+---------------+------+------+------+------+------+
| Gaz           | 7,4  | 11,1 | 11,5 | 14,7 | 14,0 |
+---------------+------+------+------+------+------+
| Énergies      | 5,2  | 4,4  | 5,0  | 4,3  | 7,0  |
| renouvelables |      |      |      |      |      |
+---------------+------+------+------+------+------+
| Charbon       | 15,5 | 16,4 | 8,4  | 4,7  | 3,4  |
+---------------+------+------+------+------+------+

1. Quel pourcentage de la consommation d’énergie le pétrole représentait-il en 1980 ?

2. À partir du tableau précédent, on a créé, pour une des années,
   un diagramme représentant la répartition des différents types d’énergie.

   Déterminer de quelle année il s’agit.

   .. image:: ../figures/exo-05-figure-01.svg

3. On peut observer l’évolution de la part du pétrole au fil des années
   à partir d’une représentation graphique comme celle proposée ci-dessous.

   .. image:: ../figures/exo-05-figure-02.svg

   Les pointillés indiquent que
   l’on suppose que la baisse de la part du pétrole va se poursuivre
   sur le rythme observé depuis 2002.

   En suivant cette supposition,
   on peut modéliser la part du pétrole (exprimée en pourcentage)
   en fonction de l’année :math:`a`
   par la fonction :math:`P`, définie ainsi :

   .. math::

      P(a) = \frac{-17}{48} a + 743,5

   a. Écrire le calcul permettant de vérifier que :math:`P(1990) ≈ 38,7` .
   b. D’après ce modèle,
      à partir de quelle année la part du pétrole sera-t-elle nulle ?
