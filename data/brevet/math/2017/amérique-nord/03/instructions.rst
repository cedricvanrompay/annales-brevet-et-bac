Il y a dans une urne 12 boules indiscernables au toucher,
numérotées de 1 à 12.
On veut tirer une boule au hasard.

1. Est-il plus probable d'obtenir un numéro pair ou bien un multiple de 3 ?

2. Quelle est la probabilité d'obtenir un numéro inférieur à 20 ?

3. On enlève de l'urne toutes les boules dont le numéro est un diviseur de 6.
   On veut à nouveau tirer une boule au hasard.

   Expliquer pourquoi la probabilité d'obtenir un numéro qui soit un nombre premier
   est alors 0,375.
