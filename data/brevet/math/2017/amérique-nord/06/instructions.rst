Le schéma ci-contre représente le jardin de Leïla.
Il n’est pas à l’échelle.
[OB] et [OF] sont des murs, OB = 6 m et OF = 4 m.

La ligne pointillée BCDEF représente
le grillage que Leïla veut installer pour délimiter
**un enclos rectangulaire OCDE**.

Elle dispose d’un rouleau de 50 m de grillage
qu’elle veut utiliser entièrement.

.. image:: ../figures/exo-06-figure-01.svg

Leïla envisage plusieurs possibilités pour placer le point C.

1. En plaçant C pour que BC = 5 m,
   elle obtient que FE = 15 m.

   a. Vérifier qu'elle les 50 m de grillage.
   b. Justifier que l'aire A de l'enclos OCDE
      est 209 m².

2. Pour avoir une aire maximale,
   Leïla fair appel à sa voisine professeur de mathématiques
   qui, un peu pressée, lui écrit sur un bout de papier:

       "En notant BC = :math:`x`,
       on a :math:`\mathbf{A}(x) = -x^2 + 18x + 144`"

   Vérifier que la formule de la voisine est bien cohérente
   avec le résultat de la question 1.

3. *Dans cette partie, les questions a) et b) ne nécessitent pas de justification*.

   a. Leïla  a saisi une formule en B2 puis l'a étirée jusqu'à la cellule I2.

      .. image:: ../figures/exo-06-figure-02.png

      Quelle formule est alors inscrite dans la cellule F2 ?

   b. Parmis les valeurs figurant dans le tableau,
      quelle est celle que Leïla va choisir pour BC
      afin d'obtenir un enclos d'aire maximale ?

   c. Donner les dimensions de l'enclos ainsi obtenu.
