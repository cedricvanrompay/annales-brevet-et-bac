Question 1
===============

Réponse B (:math:`\frac{29}{12}`)

**Note:** On fait une “mise sous le même dénominateur”,
puis on peut additionner,
ce qui donne :math:`\frac{29}{12}`.
Remarquer que :math:`\frac{29}{12}` est une fraction irréductible
(12 a 3 et 4 comme diviseurs mais 29 est premier, il n'a aucun diviseurs).


Question 2
===============

Réponse C (:math:`-1,8`)

**Note:** on a :math:`5x = 3 - 12 = -9`
donc :math:`x = - \frac{9}{5}`.
À partir de là on sait déjà  que le résultat est négatif,
donc la seule bonne réponse possible est -1,8 .
On peut tout de même vérifier que :math:`- \frac{9}{5} = -1,8`.


Question 3
===============

Réponse B (:math:`1,6`)

**Note:** À la calculette on trouve 1.61803398874989…
Arrondi au dixième près cela donne 1.6
parce que le chiffre d'après, 1, est plus petit que 5
(sinon cela aurait donné 1.7).
