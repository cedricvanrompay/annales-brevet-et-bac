Question 1
---------------

L'affranchissement d'une lettre de 20 g coûte 0,80 €.
Si les prix étaient proportionels au poids
l'affranchissement d'une lettre de 100 g, c'est à dire 5 fois plus lourde,
devrait coûter :math:`5 × 0,80 = 4` €.
Or on voit que l'affranchissement d'une lettre de 100 g coûte 1,60 €.
Donc le tarif d’affranchissement n'est pas proportionnel à la masse d’une lettre.

Question 2
---------------

Le poids total est la somme des poids
des quatres feuilles A4 et de l'enveloppe.

Une feuille A4 a une masse de 80 g par m²
et une surface de :math:`0,21 × 0,297 = 0,06237` m²,
donc une masse par feuille de :math:`80 × 0,06237 = 4,9896` g.

50 enveloppes ont une masse de 175 g
donc une enveloppe a une masse de :math:`175 / 50 = 3,5` g.

La masse totale est donc de
:math:`4 × 4,9896 + 3,5 = 23,4584` g.
La masse est entre 20 g et 100 g,
Alban doit dont payer 1,60 €.
