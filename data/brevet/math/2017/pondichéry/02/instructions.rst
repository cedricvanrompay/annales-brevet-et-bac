Un sac contient 20 boules ayant chacune la même probabilité d’être tirée.
Ces 20 boules sont numérotées de 1 à 20.
On tire une boule au hasard dans le sac.
Tous les résultats seront donnés sous forme de fractions irréductibles.

1. Quelle est la probabilité de tirer la boule numérotée 13 ?
2. Quelle est la probabilité de tirer une boule portant un numéro pair ?
3. A-t-on plus de chances d’obtenir une boule portant un numéro multiple de 4
   que d’obtenir une boule portant un numéro diviseur de 4 ?
4. Quelle est la probabilité de tirer une boule
   portant un numéro qui soit un nombre premier ?
