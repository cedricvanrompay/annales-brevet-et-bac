Question 1
------------

La probabilité de tirer la boule numérotée 13 est de
:math:`\boxed{\frac{1}{20}}`

Question 2
-----------

Il y a 10 boules portant un numéro pair (de 2 à 20),
la probabilité de tirer une boule portant un numéro pair
est donc de :math:`\frac{10}{20}` c'est à dire de :math:`\boxed{\frac{1}{2}}`

Question 3
-----------

Il y a 4 boules portant un numéro multiplie de 4 (4, 8, 12 et 16)
Et 3 boules portant un numéro diviseur de 4 (1, 2 et 4).

On a donc plus de chances d'obtenir une boule portant un numéro
:math:`\boxed{\text{multiple de 4}}`.

Question 4
----------

Il y a 8 boules portant un numéro qui est un nombre premier
(2, 3, 5, 7, 11, 13, 17 et 19),
la probabilité de tirer une boule portant un numéro qui est nombre premier
est donc de :math:`\frac{8}{20}` c'est à dire :math:`\boxed{\frac{2}{5}}`.
