Question 1
----------------------

.. math::

   E &= (x-2)(2x+3) - 3(x-2) \\
     &= 2x^2 \cancel{+3x} - 4x \cancel{-6} \cancel{-3x} \cancel{+6} \\

   \boxed{ E = 2x^2 - 4x }

Question 2
----------------------

En factorisant par :math:`2x`
on obtient:

.. math::

   E &= 2x^2 - 4x \\
     &= 2x(x-2)

   \boxed{E = 2F}

Question 3
----------------------

On cherche les valeurs de :math:`x` telles que
:math:`E = 0`, c'est à dire telles que :math:`2F = 0`,
c'est à dire telles que :math:`F = 0`.

:math:`F = x(x-2)`, or un produit de facteurs est nul
si et seulement si un de ses facteurs au moins est nul.

On a donc :math:`F = 0` si et seulement si
:math:`x = 0` ou :math:`(x-2) = 0`
c'est à dire :math:`x=2`.

Les solutions de l'équation sont donc:

.. math::

   \boxed{ x = 0 \text{ et } x = 2 }
