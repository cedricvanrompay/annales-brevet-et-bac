Pour ses 32 ans, Denis a acheté un vélo d’appartement afin de pouvoir s’entraîner pendant l’hiver.
La fréquence cardiaque (FC) est le nombre de pulsations (ou battements) du cœur par minute.

1. Denis veut estimer sa fréquence cardiaque : en quinze secondes, il a compté 18 pulsations.
   À quelle fréquence cardiaque, exprimée en pulsations par minute, cela correspond-il ?

2. Son vélo est équipé d’un cardiofréquencemètre
   qui lui permet d’optimiser son effort
   en enregistrant, dans ce cardiofréquencemètre, toutes les pulsations de son coeur.
   À un moment donné,
   le cardiofréquencemètre a mesuré un intervalle de 0,8 seconde entre deux pulsations.
   Calculer la fréquence cardiaque qui sera affichée par le cardiofréquencemètre.

3. Après une séance d’entraînement,
   le cardiofréquencemètre lui a fourni les renseignements suivants :

   +-------------------------+--------------------+-------------------+--------------------+
   | Nombre de               | Fréquence minimale | Fréquence         | Fréquence maximale |
   | pulsations enregistrées | enregistrée        | moyenne           | enregistrée        |
   +=========================+====================+===================+====================+
   | 3 640                   | 65                 | 130               | 182                |
   |                         | pulsations/minute  | pulsations/minute | pulsations/minute  |
   +-------------------------+--------------------+-------------------+--------------------+

   a. Quelle est l’étendue des fréquences cardiaques enregistrées ?
   
   b. Denis n’a pas chronométré la durée de son entraînement.
      Quelle a été cette durée ?

4. Denis souhaite connaître sa fréquence cardiaque maximale conseillée (FCMC)
   afin de ne pas la dépasser et ainsi de ménager son cœur.
   La FCMC d’un individu dépend de son âge :math:`a`, exprimé en années,
   elle peut s’obtenir grâce à la formule suivante établie par Astrand et Ryhming:

   .. math::

      \text{Fréquence cardiaque maximale conseillée} = 220 - \text{âge}

   On note :math:`f(a)` la FCMC en fonction de l’âge :math:`a`,
   on a donc :math:`f(a) = 220 - a`

   a. Vérifier que la FCMC de Denis est égale à 188 pulsations/minute.

   b. Comparer la FCMC de Denis avec la FCMC d’une personne de 15 ans.

5. Après quelques recherches,
   Denis trouve une autre formule permettant d’obtenir sa FCMC de façon plus précise.
   Si :math:`a` désigne l’âge d’un individu,
   sa FCMC peut être calculée à l’aide de la formule de Gellish :

   .. math::

      \text{Fréquence cardiaque maximale conseillée} = 191,5 − 0,007 × \text{âge}^2

   On note :math:`g(a)` la FCMC en fonction de l’âge :math:`a`,
   on a donc :math:`g(a) = 191,5 − 0,007 × a^2`

   Denis utilise un tableur
   pour comparer les résultats obtenus à l’aide des deux formules :

   +-------------------+----------------------+-------------------+
   | B2                | = 220 - A2           |                   |
   +-------------------+----------------------+-------------------+
   |   | A             | B                    | C                 |
   +---+---------------+----------------------+-------------------+
   | 1 | Âge :math:`a` | FCMC :math:`f(a)`    | FCMC :math:`g(a)` |
   |   |               | (Astrand et Ryhming) | (Gellish)         |
   +---+---------------+----------------------+-------------------+
   | 2 | 30            | 190                  | 185,2             |
   +---+---------------+----------------------+-------------------+
   | 2 | 31            | 189                  | 184,773           |
   +---+---------------+----------------------+-------------------+
   | 3 | 32            | 188                  | 184,332           |
   +---+---------------+----------------------+-------------------+
   | 4 | 33            | 187                  | 183,877           |
   +---+---------------+----------------------+-------------------+

   Quelle formule faut-il insérer dans la cellule C2 puis recopier vers le bas,
   pour pouvoir compléter la colonne « FCMC :math:`g(a)` (Gellish) » ?
