Question 1
======================

Il y a 60 secondes dans une minute;
18 pulsations en 15 secondes correspond donc à une fréquence cardiaque de:

.. math::

   18 × \frac{60}{15} = 18 × 4 = \boxed{72 \text{ pulsations / minute}}

Question 2
======================

Une pulsations toutes les 0,8 secondes correspond à
un nombre de pulsation par seconde de :math:`\frac{1}{0,8} = 1,25`,
donc à une fréquence cardiaque de

.. math::

   1,25 × 60 = \boxed{75 \text{ pulsations / minute}}

Question 3.a
======================

L'étendue des des fréquences cardiaques enregistrées est de
:math:`182 - 65 = 117` pulsations par minute.

Question 3.b
======================

Le cardiofréquencemètre a enregistré 3 640 pulsations
avec une moyenne de 130 pulsations par minute,
l'entraînement a donc duré :math:`3 640 / 130 = 28` minutes.

Question 4.a
======================

Denis a 32 ans, sa FCMC est donc de
:math:`220 - 32 = 188` pulsations par minute,
comme indiqué.

Question 4.b
======================

Une personne de 15 ans a une FCMC de
:math:`220 - 15 = 205` pulsations par minute,
supérieure donc à celle de Denis.

Question 5
======================

La formule à insérer dans le cellule C2 est:
``= 191,5 - 0,007*A2*A2``
