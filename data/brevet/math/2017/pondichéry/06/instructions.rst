On obtient la pente d'une route
en calculant le quotient du dénivelé (c’est-à-dire du déplacement vertical)
par le déplacement horizontal correspondant.
Une pente s'exprime sous forme d'un pourcentage.

Sur l'exemple ci-contre, la pente de la route est :

.. math::

   \frac{\text{dénivelé}}{\text{déplacement horizontal}} = \frac{15}{120} = 0,125 = 12,5\%

.. image:: ../figures/exo-06-figure-01.svg

Classer les pentes suivantes dans l'ordre décroissant,
c’est-à-dire de la pente la plus forte à la pente la moins forte.

+-------------------------+--------------------------------------------+
| Route descendant du     | .. image:: ../figures/exo-06-figure-02.svg |
| château des Adhémar,    |                                            |
| à Montélimar.           |                                            |
+-------------------------+--------------------------------------------+
| Tronçon d'une route     | .. image:: ../figures/exo-06-figure-03.svg |
| descendant du col du    |                                            |
| Grand Colombier (Ain).  |                                            |
+-------------------------+--------------------------------------------+
| Tronçon d'un route      | .. image:: ../figures/exo-06-figure-04.svg |
| descendant de l'Alto    |                                            |
| de l'Angliru (région    |                                            |
| des asturies, Espagne). |                                            |
+-------------------------+--------------------------------------------+
