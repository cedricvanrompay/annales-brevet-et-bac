Partie 1
=============

Question 1
-------------

La température des maquettes avant d'être mise dans la chambre froide était de 20°C.


Question 2
-------------

L'expérience a duré 100 heures, une journée dure 24 heures donc deux jours durent 48 heures, l'expérience a donc bien duré plus de deux jours.


Question 3
-------------

La maquette qui contient l'isolant le plus performant est la maquette B car c'est la dernière à atteindre les 6°C (au bout de 70h).


Partie 2
=============

Question 1
-------------

Avec un coefficient de conductivité thermique :math:`c = 0,035`
et une épaisseur :math:`e = 0,15 \text{ m}`,
la résistance thermique est de

.. math::

   R = \frac{0,15}{0,035} \approx 4.3

:math:`R` est supérieur à 4, donc la maison de Noa respecte la norme RT2012 des maisons BBC.


Question 2
-------------

On a :math:`R = \frac{e}{c}`
donc :math:`e = R × c`.

Donc une résistance thermique :math:`R = 5`
avec un coefficient de conductivité thermique :math:`c = 0,04`
correspond à une épaisseur de

.. math::

   e = 5 × 0,04 = 0,2 \text{ m}

Camille doit donc mettre une épaisseur de liège de 20 cm.
