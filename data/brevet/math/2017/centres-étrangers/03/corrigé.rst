Question 1
-----------------

.. image:: ../figures/exo-03-corrigé-figure-01.svg



Question 2
-----------------

Le volume de la pyramide est
:math:`\frac{1}{3} × \text{aire de la base} × \text{hauteur}`
où l'aire de la base est de :math:`6 × 3 = 18 \text{ cm}^2`
et la hauteur de 6 cm,
d'où un volume de
:math:`\frac{1}{3} × 18 × 6 = 36 \text{ cm}^3`

Le volume du cylindre est
:math:`π × \text{rayon}^2 × \text{hauteur}`
soit :math:`π × 2^2 × 3 \approx 38 \text{ cm}^3`

Le volume du cône est
:math:`\frac{1}{3} × π × \text{rayon}^2 × \text{hauteur}`
soit :math:`\frac{1}{3} × π × 3^2 × 3 \approx 28 \text{ cm}^3`

Enfin, le volume de la boule est
:math:`\frac{4}{3} × π × \text{rayon}^3`
soit :math:`\frac{4}{3} × π × 2^3 \approx 34 \text{ cm}^3`

On a donc le classement suivant dans l'ordre croissant des volumes:

- le cône (28 cm³)
- la boule (34 cm³)
- la pyramide (36 cm³)
- le cylindre (38 cm³)
