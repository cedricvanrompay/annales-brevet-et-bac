Pour chacune des affirmations suivantes, dire si elle est vraie ou fausse.
Chaque réponse doit être justifiée.

Affirmation 1
===============

Un menuisier prend les mesures suivantes
dans le coin d’un mur à 1 mètre au-dessus du sol
pour construire une étagère :math:`ABC` :

.. math::

   AB = 65 \text{ cm} ; AC = 72 \text{ cm et } BC = 97 \text{ cm}

.. image:: ../figures/exo-01-figure-01.svg

Il réfléchit quelques minutes et assure que l’étagère
a un angle droit.

Affirmation 2
===============

Les normes de construction imposent que
la pente d’un toit représentée ici par l’angle :math:`\widehat{CAH}`
doit avoir une mesure comprise entre 30° et 35°.

Une coupe du toit est représentée ci-dessous :
AC = 6 m et AH = 5 m.
H est le milieu de [AB].

.. image:: ../figures/exo-01-figure-02.svg

Le charpentier affirme que sa construction respecte la norme.

Affirmation 3
===============

Un peintre souhaite peindre les volets d'une maison.
Il constate qu'il utilise ⅙ du pot
pour mettre une couche de peinture sur l'intérieur et l'extérieur d'un volet.
Il doit peindre ses 4 paires de volets
et mettre sur chaque volet 3 couches de peinture.

Il affirme qu'il lui faut 2 pots de peinture.
