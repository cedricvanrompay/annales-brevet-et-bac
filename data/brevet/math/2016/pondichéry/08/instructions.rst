Afin de faciliter l’accès à sa piscine,
Monsieur Joseph décide de construire un escalier
constitué de deux prismes superposés
dont les bases sont des triangles rectangles.

Voici ses plans:

.. image:: ../figures/exo-08-figure-01.svg

**Information 1:**

- Volume du prisme = aire de la base × hauteur
- 1 L = 1 dm³

**Information 2:** Voici la reproduction d'une étiquette
figurant au dos d'un sac de ciment de 35 kg.

+--------------------+--------------+-------+------------+------+
| Dosage pour un sac | Volume de    | Sable | Gravillons | Eau  |
| de 35 kg           | béton obtenu |       |            |      |
+--------------------+--------------+-------+------------+------+
| Mortier courant    | 105 L        | ×10   |            | 16 L |
+--------------------+--------------+-------+------------+------+
| Ouvrages en béton  | 100 L        | ×5    | ×8         | 17 L |
| courant            |              |       |            |      |
+--------------------+--------------+-------+------------+------+
| Montage de murs    | 120 L        | ×12   |            | 18 L |
+--------------------+--------------+-------+------------+------+

1. Démontrer que le volume de l’escalier est égal à 1,26208 m³.

2. Sachant que l’escalier est un ouvrage en béton courant,
   déterminer le nombre de sacs de ciment de 35 kg
   nécessaires à la réalisation de l’escalier.

3. Déterminer la quantité d’eau nécessaire à cet ouvrage.
