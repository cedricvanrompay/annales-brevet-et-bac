Un confiseur lance la fabrication de bonbons au chocolat et de bonbons au caramel
pour remplir 50 boîtes.
Chaque boîte contient 10 bonbons au chocolat et 8 bonbons au caramel.

1. Combien doit-il fabriquer de bonbons de chaque sorte ?

2. Jules prend au hasard un bonbon dans une boîte.
   Quelle est la probabilité qu’il obtienne un bonbon au chocolat ?

3. Jim ouvre une autre boîte et mange un bonbon.
   Gourmand, il en prend sans regarder un deuxième.
   Est-il plus probable qu’il prenne alors un bonbon au chocolat ou un bonbon au caramel ?

4. Lors de la fabrication, certaines étapes se passent mal et, au final,
   le confiseur a 473 bonbons au chocolat et 387 bonbons au caramel.

   a) Peut-il encore constituer des boîtes contenant
      10 bonbons au chocolat et 8 bonbons au caramel
      en utilisant tous les bonbons ?
      Justifier votre réponse.

   b) Le confiseur décide de changer la composition de ses boîtes.
      Son objectif est de faire le plus de boîtes identiques possibles
      en utilisant tous ses bonbons.
      Combien peut-il faire de boîtes ?
      Quelle est la composition de chaque boîte ?
