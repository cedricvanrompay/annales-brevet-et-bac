Le tableau ci-dessous fournit le nombre d’exploitations agricoles en France,
en fonction de leur surface pour les années 2000 et 2010.

.. image:: ../figures/exo-02-figure-01.png

1. Quelles sont les catégories d’exploitations qui ont vu leur nombre augmenter
   entre 2000 et 2010 ?

2. Quelle formule doit-on saisir dans la cellule B8
   pour obtenir le nombre total d’exploitations agricoles en 2000 ?

3. Si on étire cette formule, quel résultat s’affiche dans la cellule C8 ?

4. Peut-on dire qu’entre 2000 et 2010
   le nombre d’exploitations de plus de 200 ha a augmenté de 40 % ? Justifier.
