On munit le plan complexe d’un repère orthonormé direct :math:`( O; \vec{u}, \vec{v} )`.

1. On considère l’équation :math:`(E): z^2 − 6z + c = 0`
   où :math:`c` est un réel strictement supérieur à 9.

   a. Justifier que :math:`(E)` admet deux solutions complexes non réelles.
   b. Justifier que les solutions de :math:`(E)` sont
      :math:`z_A = 3 + i \sqrt{c − 9}` et :math:`z_B = 3 − i \sqrt{c − 9}`.

2. On note A et B les points d’affixes respectives :math:`z_A` et :math:`z_B`.
   Justifier que le triangle :math:`OAB` est isocèle en :math:`O`.

3. Démontrer qu’il existe une valeur du réel :math:`c`
   pour laquelle le triangle :math:`OAB` est rectangle
   et déterminer cette valeur.
