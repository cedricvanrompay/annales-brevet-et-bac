On considère un cube ABCDEFGH représenté plus bas.

L'espace est raporté au repère
:math:`\left( A; \vec{AB}, \vec{AD}, \vec{AE} \right)` .

On note :math:`\mathscr{P}` le plan d'équation
:math:`x + \frac{1}{2} y + \frac{1}{3} z - 1 = 0`.

Construire, sur la figure fournie plus bas,
la section du cube par le plan :math:`\mathscr{P}` .

La construction devra être justifiée par des calculs ou des arguments géométriques.


Figure à compléter
====================================

.. image:: ../figures/exo-05-fig-01.svg
   :class: max-width-20-em
