Une entreprise spécialisée dans les travaux de construction
a été mandatée pour percer un tunnel à flanc de montagne.

Après étude géologique,
l’entreprise représente dans le plan la situation de la façon suivante :
dans un repère orthonormal, d’unité 2 m,
la zone de creusement est la surface délimitée par
l’axe des abscisses et la courbe :math:`\mathscr{C}`.

.. image:: ../figures/exo-03-fig-01.svg
   :class: max-width-30-em

On admet que
:math:`\mathscr{C}` est la courbe représentative de la fonction :math:`f`
définie sur l’intervalle :math:`[ −2,5 ; 2,5 ]` par :

.. math::

   f(x) = ln( −2x^2 + 13,5 )

L’objectif est de déterminer une valeur approchée, au mètre carré près,
de l’aire de la zone de creusement.


Partie A: Étude de la fonction :math:`f`
===============================================

1. Calculer :math:`f'(x)` pour :math:`x ∈ [ −2,5 ; 2,5 ]`.
2. Dresser, en justifiant, le tableau de variation de la fonction :math:`f`
   sur :math:`[ −2,5 ; 2,5 ]`.
   En déduire le signe de :math:`f` sur :math:`[ −2,5 ; 2,5 ]`.


Partie B: Aire de la zone de creusement
===============================================

On admet que
la courbe :math:`\mathscr{C}` est symétrique
par rapport à l'axe des ordonnées du repère.

1. La courbe :math:`\mathscr{C}` est-elle un arc de cercle de centre O ?
   Justifier la réponse.

2. Justifier que l’aire, en mètre carré, de la zone de creusement
   est :math:`\mathscr{A} = 8 \int_{x = 0}^{x = 2,5} f(x)\,dx`

3. L’algorithme, donné plus bas,
   permet de calculer une valeur approchée par défaut de
   :math:`I = \int_{x = 0}^{x = 2,5} f(x)\,dx`,
   notée :math:`a`.

   On admet que : :math:`a ≤ I ≤ a + \frac{f(0) - f(2,5)}{n} × 2,5`

   a. Le tableau fourni plus bas donne différentes valeurs obtenues
      pour :math:`R` et :math:`S`
      lors de l’exécution de l’algorithme pour :math:`n = 50`.
      Compléter ce tableau en calculant les six valeurs manquantes.

   b. En déduire une valeur approchée, au mètre carré près,
      de l’aire de la zone de creusement.


Annexe
===============================================

Algorithme
--------------------------------------

Variables

- :math:`R` et :math:`S` sont des réels
- :math:`n` et :math:`k` sont des entiers

Traitement

- :math:`S` prend la valeur 0
- Demander la valeur de :math:`n`
- Pour :math:`k` variant de 1 à :math:`n` faire

  - :math:`R` prend la valeur :math:`\frac{2,5}{n} × f\left(\frac{2,5}{n} × k \right)`
  - :math:`S` prend la valeur :math:`S + R`

- Fin Pour
- Afficher :math:`S`

Tableau
--------------------------------------

Le tableau ci-dessus donne les valeurs de :math:`R` et de :math:`S`,
arrondies à :math:`10^{-6}`, obtenues lors de l'exécution de l'algorithme pour :math:`n = 50`.

.. image:: ../figures/exo-03-fig-02.svg
   :class: max-width-30-em
