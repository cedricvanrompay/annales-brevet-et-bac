On considère deux suites :math:`\left( u_n \right)` et :math:`\left( v_n \right)` :

- la suite :math:`\left( u_n \right)` définie par
  :math:`u_0 = 1` et pour tout entier naturel :math:`n` :
  :math:`u_{n+1} = 2 u_n - n + 3` ;
- la suite :math:`\left( v_n \right)` définie,
  pour tout entier naturel :math:`n`,
  par :math:`v_n = 2^n` .


Partie A: Conjectures
=======================================================================

Florent a calculé les premiers termes de ces deux suites à l’aide d’un tableur.
Une copie d’écran est donnée ci-dessous.

.. image:: ../figures/exo-04-fig-01.png
   :class: max-width-30-em

1. Quelles formules ont été entrées dans les cellules B3 et C3
   pour obtenir par copie vers le bas les termes des deux suites ?

2. Pour les termes de rang 10, 11, 12 et 13 Florent obtient les résultats suivants :

    .. image:: ../figures/exo-04-fig-02.png
       :class: max-width-30-em

   Conjecturer les limites des suites
   :math:`\left( u_n \right)` et :math:`\left( \frac{u_n}{v_n} \right)`.


Partie B: Étude de la suite :math:`\left( u_n \right)`
=======================================================================

1. Démontrer par récurrence que, pour tout entier naturel :math:`n`,
   on a :math:`u_n = 3 × 2^n + n − 2` .

2. Déterminer la limite de la suite :math:`\left( u_n \right)` .

3. Déterminer le rang du premier terme de la suite supérieur à 1 million.


Partie C: Étude de la suite :math:`\left( \frac{u_n}{v_n} \right)`
=======================================================================

1. Démontrer que la suite :math:`\left( \frac{u_n}{v_n} \right)`
   est décroissante à partir du rang 3.

2. On admet que, pour tout entier :math:`n` supérieur ou égal à 4,
   on a : :math:`0 < \frac{n}{2^n} ≤ \frac{1}{n}` .

   Déterminer la limite de la suite :math:`\left( \frac{u_n}{v_n} \right)` .
