Les parties A, B et C peuvent être traitées de façon indépendante.
Dans tout l’exercice, les résultats seront arrondis, si nécessaire, au millième.

La chocolaterie « Choc’o » fabrique des tablettes de chocolat noir,
de 100 grammes, dont la teneur en cacao annoncée est de 85 %.

Partie A
====================

À l’issue de la fabrication,
la chocolaterie considère que certaines tablettes ne sont pas commercialisables :
tablettes cassées, mal emballées, mal calibrées, etc.
La chocolaterie dispose de deux chaînes de fabrication :

- la chaîne A, lente,
  pour laquelle la probabilité qu’une tablette de chocolat soit commercialisable est égale à 0,98.

- la chaîne B, rapide,
  pour laquelle la probabilité qu’une tablette de chocolat soit commercialisable est 0,95.

À la fin d’une journée de fabrication, on prélève au hasard une tablette et on note :

- A l’ évènement : « la tablette de chocolat provient de la chaîne de fabrication A » ;
- C l’évènement : « la tablette de chocolat est commercialisable ».

On note x la probabilité qu’une tablette de chocolat provienne de la chaîne A.

1. Montrer que :math:`P(C) = 0,03x + 0,95`
2. À l’issue de la production,
   on constate que 96 % des tablettes sont commercialisables
   et on retient cette valeur pour modéliser la probabilité qu’une tablette soit commercialisable.
   Justifier que la probabilité que la tablette provienne de la chaîne B
   est deux fois égale à celle que la tablette provienne de la chaîne A.


Partie B
====================

Une machine électronique mesure la teneur en cacao d’une tablette de chocolat.
Sa durée de vie, en années, peut être modélisée par une variable aléatoire Z
suivant une loi exponentielle de paramètre :math:`λ`.

1. La durée de vie moyenne de ce type de machine est de 5 ans.
   Déterminer le paramètre  de la loi exponentielle.
2. Calculer :math:`P(Z>2)`.
3. Sachant que la machine de l’atelier a déjà fonctionné pendant 3 ans,
   quelle est la probabilité que sa durée de vie dépasse 5 ans ?


Partie C
====================

On note :math:`X` la variable aléatoire donnant la teneur en cacao, exprimée en pourcentage,
d’une tablette de 100 g de chocolat commercialisable.
On admet que :math:`X` suit la loi normale d’espérance :math:`μ = 85`
et d’écart type :math:`σ = 2` .

1. Calculer :math:`P(83 ≤ X ≤ 87)`.
   Quelle est la probabilité que la teneur en cacao
   soit différente de plus de 2 % du pourcentage annoncé sur l’emballage ?
2. Déterminer une valeur approchée au centième du réel :math:`a` tel que :
   :math:`P(85-a ≤ X ≤ 85+a) = 0,9`.
   Interpréter le résultat dans le contexte de l’exercice.
3. La chocolaterie vend un lot de 10 000 tablettes de chocolat
   à une enseigne de la grande distribution.
   Elle affirme au responsable achat de l’enseigne que,
   dans ce lot, 90 % des tablettes ont un pourcentage de cacao
   appartenant à l’intervalle :math:`[ 81,7 ; 88,3 ]`.

   Afin de vérifier si cette affirmation n’est pas mensongère,
   le responsable achat fait prélever 550 tablettes au hasard dans le lot et constate que,
   sur cet échantillon, 80 ne répondent pas au critère.

   Au vu de l’échantillon prélevé,
   que peut-on conclure quant à l’affirmation de la chocolaterie ?
